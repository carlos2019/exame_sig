<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComunicationEmailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comunication_email', function (Blueprint $table) {
            $table->increments('id_communication_email');
            $table->text('body_email')->nullable(true);
            $table->dateTime('date_email')->nullable(true);
            $table->integer('type_direction')->nullable(true);
            $table->boolean('status_email')->nullable(true);
            $table->integer('id_room')->nullable(true)->unsigned();
            $table->integer('id_account')->nullable(true)->unsigned();
            $table->bigInteger('id_user')->nullable(true)->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comunication_email');
    }
}
