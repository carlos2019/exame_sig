<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account', function (Blueprint $table) {
            $table->increments('id_account');
            $table->string('name_account',100)->nullable(true);
            $table->boolean('active_account')->nullable(true);
            $table->string('zip_account',100)->nullable(true);
            $table->integer('id_time_zone')->nullable(true)->unsigned();
            $table->integer('id_comunication')->nullable(true)->unsigned();
            $table->integer('id_state')->nullable(true)->unsigned();
            $table->integer('id_country')->nullable(true)->unsigned();
            $table->integer('id_type_account')->nullable(true)->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account');
    }
}
