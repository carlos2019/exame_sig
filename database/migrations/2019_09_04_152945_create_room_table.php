<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room', function (Blueprint $table) {
            $table->increments('id_room');
            $table->string('city_room',250)->nullable(true);
            $table->string('address_room',250)->nullable(true);
            $table->string('coordenates',250)->nullable(true);
            $table->text('img_room')->nullable(true);
            $table->text('description')->nullable(true);
            $table->integer('id_account')->nullable(true)->unsigned();
            $table->integer('id_state')->nullable(true)->unsigned();
            $table->integer('id_country')->nullable(true)->unsigned();
            $table->integer('id_reputation')->nullable(true)->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room');
    }
}
